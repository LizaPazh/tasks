﻿using BLL.DTOs;
using DAL.Models;
using Newtonsoft.Json;
using Project_Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace IntegrationTests
{
    public class TasksControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;
        private const string BaseUrl = "http://localhost:44392/api";
        public TasksControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public async Task DeleteTask_ThenResponseCode204AndGetByIdResponse404()
        {
            var task = new TaskDTO
            {
                Id = 203,
                Name = "Necessitatibus beatae animi unde velit nisi.",
                Description = "Dolorem ut eligendi blanditiis est labore. Quaerat voluptas reiciendis provident. Quae aperiam neque nemo omnis. Dicta suscipit possimus. Qui voluptatem tenetur. Est sequi corrupti.",
                CreatedAt = new DateTime(2020, 06, 01, 13, 59, 44, 16),
                State = TaskState.Started,
                ProjectId = 6,
                PerformerId = 1
            };

            string json = JsonConvert.SerializeObject(task);
            await _client.PostAsync($"{BaseUrl}/tasks", new StringContent(json, Encoding.UTF8, "application/json"));

            var httpDeleteResponse = await _client.DeleteAsync($"{BaseUrl}/tasks/" + task.Id);

            Assert.Equal(HttpStatusCode.NoContent, httpDeleteResponse.StatusCode);

            var httpResponseGetById = await _client.GetAsync($"{BaseUrl}/tasks/" + task.Id);

            Assert.Equal(HttpStatusCode.NotFound, httpResponseGetById.StatusCode);
            
        }

        [Fact]
        public async Task DeleteTask_WhenTaskDoesntExist_ThenResponseCode404()
        {
            int taskId = 10000;
            var httpResponse = await _client.DeleteAsync($"{BaseUrl}/tasks" + taskId);
            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
        }

        [Fact]
        public async Task CreateTask_ThenResponseWithCode200AndCorrespondedBody()
        {
            var task = new TaskDTO
            {
                Id = 220,
                Name = "Necessitatibus beatae animi unde velit nisi.",
                Description = "Dolorem ut eligendi blanditiis est labore. Quaerat voluptas reiciendis provident. Quae aperiam neque nemo omnis. Dicta suscipit possimus. Qui voluptatem tenetur. Est sequi corrupti.",
                CreatedAt = new DateTime(2020, 06, 01, 13, 59, 44, 16),
                State = TaskState.Started,
                ProjectId = 6,
                PerformerId = 1
            };

            string json = JsonConvert.SerializeObject(task);
            var httpResponse = await _client.PostAsync($"{BaseUrl}/tasks", new StringContent(json, Encoding.UTF8, "application/json"));

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var createdTask= JsonConvert.DeserializeObject<TaskDTO>(stringResponse);

            await _client.DeleteAsync($"{BaseUrl}/tasks/" + task.Id);

            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.Equal(task.Id, createdTask.Id);
            Assert.Equal(task.Name, createdTask.Name);

        }
        [Fact]
        public async Task UnFinishedTasksByUser_WhenUserWithId1_ThenReturn2TasksWithFirstId117()
        {
            int userId = 1;
            var httpResponse = await _client.GetAsync($"{BaseUrl}/tasks/unfinished/" + userId);

            var response = await httpResponse.Content.ReadAsStringAsync();
            var tasks = JsonConvert.DeserializeObject<List<TaskDTO>>(response);
            

            Assert.Equal(userId, tasks.FirstOrDefault().PerformerId);
            Assert.Equal(2, tasks.Count());
            Assert.Equal(117, tasks[0].Id);
            Assert.Equal(177, tasks[1].Id);
        }

        [Fact]
        public async Task UnFinishedTasksByUser_WhenUserWithIdDoesntExist_ThenExceptionThrown()
        {
            int userId = 1000;
            var httpResponse = await _client.GetAsync($"{BaseUrl}/tasks/unfinished/" + userId);

            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
        }      
    }
}
