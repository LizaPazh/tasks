﻿using AutoMapper;
using BLL.DTOs;
using BLL.Exceptions;
using BLL.Interfaces;
using DAL.Models;
using DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class TaskService: ITaskService
    {
        private readonly IProjectTaskRepository _taskRep;
        private readonly IUserRepository _userRep;
        private readonly IMapper _mapper;
        public TaskService(IProjectTaskRepository rep, IMapper mapper, IUserRepository userRepository)
        {
            _taskRep = rep;
            _mapper = mapper;
            _userRep = userRepository;
        }
        public async Task<TaskDTO> GetTaskById(int taskId)
        {
            var task = await _taskRep.GetById(taskId);
            if (task == null)
            {
                throw new NotFoundException($"Task with id: {taskId} not found");
            }
            return _mapper.Map<TaskDTO>(task);
        }
        public async Task<IEnumerable<TaskDTO>> GetAllTasks()
        {
            var tasks = await _taskRep.GetAll();
            return _mapper.Map<IEnumerable<TaskDTO>>(tasks);
        }
        public async Task CreateTask(TaskDTO taskDTO)
        {
            if (taskDTO == null)
            {
                throw new ArgumentNullException();
            }
            var task = _mapper.Map<ProjectTask>(taskDTO);
            await _taskRep.Create(task);
        }
        public async Task UpdateTask(TaskDTO taskDTO)
        {
            var existingTask = await _taskRep.GetById(taskDTO.Id);

            if (existingTask == null)
            {
                throw new NotFoundException($"Task with id: {taskDTO.Id} not found");
            }

            var task = _mapper.Map<ProjectTask>(taskDTO);
            await _taskRep.Update(task);
        }
        public async Task DeleteTask(int taskId)
        {
            var task = await _taskRep.GetById(taskId);
            if (task == null)
            {
                throw new NotFoundException($"Task with id: {taskId} not found");
            }
            await _taskRep.Delete(taskId);
        }
        public async Task<IEnumerable<TaskDTO>> TasksByUser(int userId)
        {
            var user = await _userRep.GetById(userId);
            if (user == null)
            {
                throw new NotFoundException($"User with {userId} not found");
            }
            return _mapper.Map<IEnumerable<TaskDTO>>((await _taskRep.GetAll()).Where(t => t.PerformerId == userId && t.Name.Length < 45));
        }
        public async Task<IEnumerable<Tuple<int, string>>> FinishedTasksByUser(int userId)
        {
            var user = await _userRep.GetById(userId);
            if (user == null)
            {
                throw new NotFoundException($"User with {userId} not found");
            }
            return (await _taskRep.GetAll()).Where(t => t.PerformerId == userId && t.FinishedAt.Year == DateTime.Now.Year)
                         .Select(c => new Tuple<int, string>(c.Id, c.Name));
        }

        public async Task<IEnumerable<TaskDTO>> UnFinishedTasksByUser(int userId)
        {
            var user = await _userRep.GetById(userId);
            if(user == null)
            {
                throw new NotFoundException($"User with {userId} not found");
            }
            var tasks =  (await _taskRep.GetAll()).Where(t => t.PerformerId == userId && t.State != TaskState.Finished);
            return _mapper.Map<IEnumerable<TaskDTO>>(tasks);
        }
    }
}
