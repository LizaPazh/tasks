﻿using BLL.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface ITeamService
    {
        Task<TeamDTO> GetTeamById(int teamId);
        Task<IEnumerable<TeamDTO>> GetAllTeams();
        Task CreateTeam(TeamDTO team);
        Task UpdateTeam(TeamDTO team);
        Task DeleteTeam(int teamId);
        Task<IEnumerable<TeamWithUsers>> GetTeamsByUserAge();
    }
}
