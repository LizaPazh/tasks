﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.DTOs;
using BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Project_Structure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITaskService service;
        public TasksController(ITaskService taskService)
        {
            service = taskService;
        }

        [HttpGet]
        public async Task<IActionResult> GetTasks()
        {
            return Ok((await service.GetAllTasks()));
        }

        [HttpGet("{taskId}")]
        public async Task<IActionResult> GetTaskById(int taskId)
        {
            var task = await service.GetTaskById(taskId);
            return Ok(task);
        }
        [HttpPost]
        public async Task<IActionResult> CreateTask(TaskDTO task)
        {
            await service.CreateTask(task);
            return Ok(task);
        }
        [HttpPut]
        public async Task<IActionResult> UpdateTask(TaskDTO task)
        {
            await service.UpdateTask(task);
            return NoContent();
        }

        [HttpDelete("{taskId}")]
        public async Task<IActionResult> DeleteTask(int taskId)
        {
            await service.DeleteTask(taskId);
            return NoContent();
        }

        [HttpGet("tasksByUser/{userId}")]
        public async Task<IActionResult> TasksByUser(int userId)
        {
            return Ok(await service.TasksByUser(userId));
        }

        [HttpGet("finished/{userId}")]
        public async Task<IActionResult> FinishedThisYearTasksByUser(int userId)
        {
            return Ok(await service.FinishedTasksByUser(userId));
        }

        [HttpGet("unFinished/{userId}")]
        public async Task<IActionResult> UnFinishedTasksByUser(int userId)
        {
            return Ok(await service.UnFinishedTasksByUser(userId));
        }
    }
}