﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.DTOs;
using BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Project_Structure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {

        private readonly ITeamService service;
        public TeamsController(ITeamService teamService)
        {
            service = teamService;
        }

        [HttpGet]
        public async Task<IActionResult> GetTeams()
        {
            return Ok(await service.GetAllTeams());
        }

        [HttpGet("{teamId}")]
        public async Task<IActionResult> GetTeamById(int teamId)
        {
            var team = await service.GetTeamById(teamId);
            return Ok(team);
        }

        [HttpPost]
        public async Task<IActionResult> CreateTeam(TeamDTO team)
        {
            await service.CreateTeam(team);
            return Ok(team);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateTeam(TeamDTO team)
        {
            await service.UpdateTeam(team);
            return NoContent();
        }

        [HttpDelete("{teamId}")]
        public async Task<IActionResult> DeleteTeam(int teamId)
        {
            await service.DeleteTeam(teamId);
            return NoContent();
        }

        [HttpGet("teamsByUserAge")]
        public async Task<IActionResult> GetTeamsByUserAge()
        {
            return Ok(await service.GetTeamsByUserAge());
        }
    }
}