﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.DTOs;
using BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;

namespace Project_Structure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectService service;
        public ProjectsController(IProjectService projectService)
        {
            service = projectService;
        }

        [HttpGet]
        public async Task<IActionResult> GetProjects()
        {
            return Ok(await service.GetAllProjects());
        }

        [HttpGet("{projectId}")]
        public async Task<IActionResult> GetProjectById(int projectId)
        {
            var project = await service.GetProjectById(projectId);
            return Ok(project);
        }

        [HttpPost]
        public async Task<IActionResult> CreateProject(ProjectDTO project)
        {
            await service.CreateProject(project);
            return Ok(project);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateProject(ProjectDTO project)
        {
            await service.UpdateProject(project);
            return NoContent();
        }

        [HttpDelete("{projectId}")]
        public async Task<IActionResult> DeleteProject(int projectId)
        {
            await service.DeleteProject(projectId);
            return NoContent();
        }

        [HttpGet("tasks/{userId}")]
        public async Task<IActionResult> CountTasksForProjectByUser(int userId)
        {
            return  Ok((await service.CountTasksForProjectByUser(userId)).ToList());
        }

        [HttpGet("projectsWithCharacteristics")]
        public async Task<IActionResult> GetProjectWithCharacteristics()
        {
            return Ok(await service.GetProjectWithCharacteristics());
        }
    }
}