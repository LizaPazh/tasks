﻿using BLL.DTOs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Client_App
{
    public class QueryService
    {
        readonly HttpClient client = new HttpClient() { BaseAddress = new Uri("https://localhost:44392/api/") };

        public Task<int> MarkRandomTaskWithDelay(int delay)
        {
            TaskCompletionSource<int> tcs = new TaskCompletionSource<int>();
            Timer t = new Timer();
            t.Interval = delay;
            t.Elapsed += async (sender, e) => await HandleUpdateTask(t, tcs);
            t.Start();
            return tcs.Task;
        }

        public async Task<List<KeyValuePair<ProjectDTO, int>>> CountTasksForProjectByUser(int userId)
        {
            var response = await client.GetStringAsync($"projects/tasks/{userId}");
            return JsonConvert.DeserializeObject<List<KeyValuePair<ProjectDTO, int>>>(response);
        }
        public async Task<IEnumerable<TaskDTO>> TasksByUser(int userId)
        {
            var response = await client.GetStringAsync($"tasks/tasksByUser/{userId}");
            return JsonConvert.DeserializeObject<IEnumerable<TaskDTO>>(response);
        }

        public async Task<IEnumerable<Tuple<int, string>>> FinishedTasksByUser(int userId)
        {
            var response = await client.GetStringAsync($"tasks/finished/{userId}");
            return JsonConvert.DeserializeObject<IEnumerable<Tuple<int, string>>>(response);
        }

        public async Task<IEnumerable<TeamWithUsers>> GetTeamsByUserAge()
        {
            var response = await client.GetStringAsync($"teams/teamsByUserAge");
            return JsonConvert.DeserializeObject<IEnumerable<TeamWithUsers>>(response);
        }
        public async Task<IEnumerable<UserDTO>> UsersByABCWithTasksSortedByName()
        {
            var response = await client.GetStringAsync($"users/usersByABCWithSortedTasks");
            return JsonConvert.DeserializeObject<IEnumerable<UserDTO>>(response);
        }
        public async Task<UserWithParameters> GetUsersCharacteristics(int userId)
        {
            var response = await client.GetStringAsync($"users/GetUsersCharacteristics/{userId}");
            return JsonConvert.DeserializeObject<UserWithParameters>(response);
        }
        public async Task<IEnumerable<ProjectWithParameters>> GetProjectWithCharacteristics()
        {
            var response = await client.GetStringAsync($"projects/projectsWithCharacteristics");
           return JsonConvert.DeserializeObject<IEnumerable<ProjectWithParameters>>(response);
        }

        private async Task HandleUpdateTask(Timer t, TaskCompletionSource<int> tcs)
        {
            t.Stop();
            var response = await client.GetStringAsync($"tasks");
            var tasks = JsonConvert.DeserializeObject<IEnumerable<TaskDTO>>(response);
            var unfinishedTasks = tasks.Where(t => t.State != TaskState.Finished);
            if (!unfinishedTasks.Any())
            {
                tcs.SetException(new Exception("Unfinished tasks not exists"));
            }

            Random rand = new Random();
            int index = rand.Next(unfinishedTasks.Count());

            var taskToUpdate = tasks.ElementAt(index);
            var jsonString = JsonConvert.SerializeObject(taskToUpdate);
            var httpContent = new StringContent(jsonString, Encoding.UTF8, "application/json");

            var updateResponse = await client.PutAsync("tasks", httpContent);

            if (updateResponse.IsSuccessStatusCode)
            {
                tcs.SetResult(index);
            }

            if (!updateResponse.IsSuccessStatusCode)
            {
                var errorMessage = await updateResponse.Content.ReadAsStringAsync();

                tcs.SetException(new Exception(errorMessage));
            }
        }
    }
}
