﻿using Client_App;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BSA_LINQ
{
    class Program
    {
       
        static async Task Main(string[] args)
        {
            QueryService service = new QueryService();
            bool showMenu = true;
            while (showMenu)
            {
                showMenu = await MainMenu(service);
            }
            Console.ReadLine();
        }

        private static async Task<bool> MainMenu(QueryService service)
        {
            Console.WriteLine();
            Console.WriteLine("Choose an option:");
            Console.WriteLine("1) Mark random task as completed");
            Console.WriteLine("2) Get the number of tasks from the project of a specific user");
            Console.WriteLine("3) Get the list of tasks assigned to a specific user(by id), where name task is < 45 characters");
            Console.WriteLine("4) Get the list (id, name) from the collection of tasks that were completed in the current (2020) year for " +
                "a specific user (by id).");
            Console.WriteLine("5) Get the list from the collection of teams whose members are older than 10 years, " +
                "sorted by user registration date in descending order, as well as grouped by team.");
            Console.WriteLine("6) Get a list of users alphabetically first_name (ascending) with sorted tasks by length name (descending).");
            Console.WriteLine("7) Get the following structure (pass the user Id to the parameters): User, Last user project(by creation date), " +
                "Total number of tasks under the last project, The total number of incomplete or canceled tasks for the user, " +
                "The longest user task by date");
            Console.WriteLine("8) Get the following structure: Project, The longest project task, The shortest project task(by name), " +
                "The total number of users in the project team, where either the project description > 20 characters or the number of tasks < 3");
            Console.WriteLine("9) Exit");
            Console.WriteLine("Choose an option: ");

            switch (Console.ReadLine())
            {
                case "1":       
                    CallUpdateTaskAsync(service);
                    return true;
                case "2":
                    Console.WriteLine("Input user id:");
                    int userId;
                    userId = Convert.ToInt32(Console.ReadLine());
                    var userProjectsTasks = await service.CountTasksForProjectByUser(userId);
                    foreach (var item in userProjectsTasks)
                    {
                        Console.WriteLine($"Project name: {item.Key.Name}  Number of tasks: {item.Value}");
                    }
                    return true;
                case "3":
                    Console.WriteLine();
                    Console.WriteLine("Input user id:");
                    userId = Convert.ToInt32(Console.ReadLine());
                    var tasks = await service.TasksByUser(userId);

                    foreach (var task in tasks)
                    {
                        Console.WriteLine($"Task id: {task.Id}, task name: {task.Name}");
                    }
                    return true;
                case "4":
                    Console.WriteLine();                 
                    Console.WriteLine("Input user id:");
                    userId = Convert.ToInt32(Console.ReadLine());
                    var finishedTasks = await service.FinishedTasksByUser(userId);
                    foreach (var task in finishedTasks)
                    {
                        Console.WriteLine($"Task id: {task.Item1}, task name: {task.Item2}");
                    }
                    return true;
                case "5":
                    Console.WriteLine();
                    Console.WriteLine("Get the list from the collection of teams whose members are older than 10 years, " +
                        "sorted by user registration date in descending order, as well as grouped by team.");

                    var teams = await service.GetTeamsByUserAge();

                    foreach (var team in teams)
                    {
                        Console.WriteLine($"Team id: {team.TeamId}, team name: {team.TeamName}, team users count: {team.Users.Count()}");
                    }
                    return true;
                case "6":
                    Console.WriteLine();
                    var users = await service.UsersByABCWithTasksSortedByName();
                    foreach (var user in users)
                    {
                        Console.WriteLine($"User name: {user.FirstName}");
                        Console.WriteLine("User tasks:");
                        foreach (var t in user.ProjectTasks)
                        {
                            Console.WriteLine($"Task name: {t.Name}");
                        }
                    }
                    return true;
                case "7":
                    Console.WriteLine();
                    Console.WriteLine("Input user id:");
                    userId = Convert.ToInt32(Console.ReadLine());
                    var userWithParams = await service.GetUsersCharacteristics(userId);
                    Console.WriteLine($"User name: {userWithParams.User.FirstName}, last project id: {userWithParams.LastProject?.Id}, " +
                        $"last project tasks count: {userWithParams.LastProjectTasks}, unfinishedUserTasks: {userWithParams.NotCompletedOrCanceledTasks}," +
                        $" longest task id: {userWithParams.LongestTask?.Id} ");
                    return true;
                case "8":
                    Console.WriteLine();
                    var projectsWithParams = await service.GetProjectWithCharacteristics();
                    foreach (var p in projectsWithParams)
                    {
                        Console.WriteLine($"Project id: {p.Project.Id}, longest task id: {p.TaskByDescription?.Id}, " +
                            $"shortest project task id: {p.TaskByName?.Id}, number of users in the project team: {p.UsersCount} ");
                    }
                    return true;
                case "9":
                    return false;
                default:
                    return true;
            }
        }

        static async void CallUpdateTaskAsync(QueryService service)
        {
            var markedTaskId = await service.MarkRandomTaskWithDelay(1000);
            Console.WriteLine();
            Console.WriteLine($"Task marks as completed with id: {markedTaskId}");
        }
    }
}
